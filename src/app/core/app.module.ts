import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderModule } from '../shared/header/header.module';
import { HeroSectionModule } from '../home/hero-section/hero-section.module';
import { FooterModule } from '../shared/footer/footer.module';
import { ValuePropositionModule } from '../home/value-proposition/value-proposition.module';
import { BenefitsSectionModule } from '../home/benefits-section/benefits-section.module';
import { LoggerServiceService } from '../services/logger/logger-service.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeaderModule,
    HeroSectionModule,
    ValuePropositionModule,
    BenefitsSectionModule,
    FooterModule
  ],
  providers: [LoggerServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {}
