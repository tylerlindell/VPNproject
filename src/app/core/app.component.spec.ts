import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '../../../node_modules/@angular/platform-browser';
import { HeaderModule } from '../shared/header/header.module';
import { HeroSectionModule } from '../home/hero-section/hero-section.module';
import { ValuePropositionModule } from '../home/value-proposition/value-proposition.module';
import { BenefitsSectionModule } from '../home/benefits-section/benefits-section.module';
import { FooterModule } from '../shared/footer/footer.module';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HeaderModule,
        HeroSectionModule,
        ValuePropositionModule,
        BenefitsSectionModule,
        FooterModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'SerpentVPN Official Website | Best VPN Provider for a Private Internet'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('SerpentVPN Official Website | Best VPN Provider for a Private Internet');
  }));
});
