import { Component, AfterViewInit } from '@angular/core';
import { LoggerServiceService } from '../services/logger/logger-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'SerpentVPN Official Website | Best VPN Provider for a Private Internet';

  constructor(private loggerServiceService: LoggerServiceService) {}

  ngAfterViewInit() {
    this.loggerServiceService.init();
  }
}
