import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroSectionComponent } from './hero-section.component';
import { HeroImageSectionComponent } from './hero-image-section/hero-image-section.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HeroSectionComponent, HeroImageSectionComponent],
  exports: [
    HeroSectionComponent
  ]
})
export class HeroSectionModule { }
