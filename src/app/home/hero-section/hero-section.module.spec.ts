import { HeroSectionModule } from './hero-section.module';

describe('HeroSectionModule', () => {
  let heroSectionModule: HeroSectionModule;

  beforeEach(() => {
    heroSectionModule = new HeroSectionModule();
  });

  it('should create an instance', () => {
    expect(heroSectionModule).toBeTruthy();
  });
});
