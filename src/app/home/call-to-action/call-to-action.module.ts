import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CallToActionComponent } from './call-to-action.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CallToActionComponent],
  exports: [
    CallToActionComponent
  ]
})
export class CallToActionModule { }
