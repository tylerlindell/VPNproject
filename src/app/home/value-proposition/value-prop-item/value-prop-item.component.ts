import { Component, OnInit, Input } from '@angular/core';
import { ValueProposition } from '../../../models/value-proposition.model';

@Component({
  selector: 'app-value-prop-item',
  templateUrl: './value-prop-item.component.html',
  styleUrls: ['./value-prop-item.component.scss']
})
export class ValuePropItemComponent implements OnInit {
  @Input() public prop: ValueProposition;

  constructor() {}

  ngOnInit() {}
}
