import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuePropItemComponent } from './value-prop-item.component';

describe('ValuePropItemComponent', () => {
  let component: ValuePropItemComponent;
  let fixture: ComponentFixture<ValuePropItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValuePropItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuePropItemComponent);
    component = fixture.componentInstance;
    component.prop = {
      image: '../../assets/icon_infrastructure.svg',
      title: 'Lock Down Cloud Infrastructure',
      body: 'Reduce the number of attack vectors on your public cloud infrastructure.'
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
