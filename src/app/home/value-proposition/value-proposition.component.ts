import { Component, OnInit } from '@angular/core';
import { ValueProposition } from '../../models/value-proposition.model';

@Component({
  selector: 'app-value-proposition',
  templateUrl: './value-proposition.component.html',
  styleUrls: ['./value-proposition.component.scss']
})
export class ValuePropositionComponent implements OnInit {
  public valueProps: ValueProposition[] = [
    {
      image: '../../assets/icon_infrastructure.svg',
      title: 'Lock Down Cloud Infrastructure',
      body: 'Reduce the number of attack vectors on your public cloud infrastructure.'
    },
    {
      image: '../../assets/icon_vulnerabilities.svg',
      title: 'Protect Against Vulnerabilities',
      body: 'Protect your cloud servers against scanning for open SSH vulnerabilities and logins.'
    },
    {
      image: '../../assets/icon_third_party.svg',
      title: 'Secure Third-Party Services',
      body: 'Lock down access to third-party services, such as Salesforce, to your servers\' static IP address.'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
