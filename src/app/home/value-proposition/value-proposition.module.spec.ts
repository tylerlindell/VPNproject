import { ValuePropositionModule } from './value-proposition.module';

describe('ValuePropositionModule', () => {
  let valuePropositionModule: ValuePropositionModule;

  beforeEach(() => {
    valuePropositionModule = new ValuePropositionModule();
  });

  it('should create an instance', () => {
    expect(valuePropositionModule).toBeTruthy();
  });
});
