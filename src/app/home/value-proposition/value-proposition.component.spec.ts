import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuePropositionComponent } from './value-proposition.component';
import { ValuePropItemComponent } from './value-prop-item/value-prop-item.component';

describe('ValuePropositionComponent', () => {
  let component: ValuePropositionComponent;
  let fixture: ComponentFixture<ValuePropositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ValuePropositionComponent, ValuePropItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuePropositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
