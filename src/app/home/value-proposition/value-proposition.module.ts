import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValuePropositionComponent } from './value-proposition.component';
import { ValuePropItemComponent } from './value-prop-item/value-prop-item.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ValuePropositionComponent, ValuePropItemComponent],
  exports: [
    ValuePropositionComponent
  ]
})
export class ValuePropositionModule { }
