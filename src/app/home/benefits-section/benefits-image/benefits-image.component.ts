import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-benefits-image',
  templateUrl: './benefits-image.component.html',
  styleUrls: ['./benefits-image.component.css']
})
export class BenefitsImageComponent implements OnInit {

  @Input() public img: string;
  @Input() public title: string;

  constructor() {}

  ngOnInit() {}
}
