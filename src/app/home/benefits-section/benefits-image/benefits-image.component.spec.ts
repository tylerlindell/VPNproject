import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsImageComponent } from './benefits-image.component';

describe('BenefitsImageComponent', () => {
  let component: BenefitsImageComponent;
  let fixture: ComponentFixture<BenefitsImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitsImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
