import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-benefit-value-prop-list',
  templateUrl: './benefit-value-prop-list.component.html',
  styleUrls: ['./benefit-value-prop-list.component.scss']
})
export class BenefitValuePropListComponent implements OnInit {

  @Input() public valueList: string[];
  @Input() public icon: string;

  constructor() { }

  ngOnInit() {
  }

}
