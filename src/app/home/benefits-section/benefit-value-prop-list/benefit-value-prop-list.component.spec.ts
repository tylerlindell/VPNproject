import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitValuePropListComponent } from './benefit-value-prop-list.component';

describe('BenefitValuePropListComponent', () => {
  let component: BenefitValuePropListComponent;
  let fixture: ComponentFixture<BenefitValuePropListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitValuePropListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitValuePropListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
