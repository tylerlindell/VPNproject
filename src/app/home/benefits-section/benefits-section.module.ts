import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BenefitsSectionComponent } from './benefits-section.component';
import { BenefitsImageComponent } from './benefits-image/benefits-image.component';
import { BenefitValuePropListComponent } from './benefit-value-prop-list/benefit-value-prop-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BenefitsSectionComponent, BenefitsImageComponent, BenefitValuePropListComponent],
  exports: [
    BenefitsSectionComponent
  ]
})
export class BenefitsSectionModule { }
