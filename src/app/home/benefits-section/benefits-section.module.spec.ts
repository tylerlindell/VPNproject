import { BenefitsSectionModule } from './benefits-section.module';

describe('BenefitsSectionModule', () => {
  let benefitsSectionModule: BenefitsSectionModule;

  beforeEach(() => {
    benefitsSectionModule = new BenefitsSectionModule();
  });

  it('should create an instance', () => {
    expect(benefitsSectionModule).toBeTruthy();
  });
});
