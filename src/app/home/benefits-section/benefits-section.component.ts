import { Component, OnInit } from '@angular/core';
import { Benefit } from '../../models/benefit.model';

@Component({
  selector: 'app-benefits-section',
  templateUrl: './benefits-section.component.html',
  styleUrls: ['./benefits-section.component.scss']
})
export class BenefitsSectionComponent implements OnInit {

  public benefits: Benefit[] = [
    {
      image: '../../assets/secure.svg',
      title: 'Secure Access Point',
      body: 'Serpent VPN provides a secure access point that locks down your cloud' +
      'infrastructure and protects your Internet connection. With Serpent VPN, you can:',
      list: [
        'Lock Down Cloud Infrastructure',
        'Protect Against Vulnerabilities',
        'Secure Third-Party Services',
        'Secure Every Internet Connection'
      ]
    },
    {
      image: '../../assets/advantages.svg',
      title: 'The Advantages are Endless',
      body: 'Serpent VPN makes using a VPN simple and streamlined while also offering several cool benefits:',
      list: [
        'Use your own dedicated IP address from a server you manage',
        'Set your own logging policy',
        'Choose your own hardware',
        'Set up within minutes',
        'Retain access to our global server network',
      ],
      listIcon: '../../assets/checkmark.svg'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
