import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsSectionComponent } from './benefits-section.component';
import { BenefitsImageComponent } from './benefits-image/benefits-image.component';
import { BenefitValuePropListComponent } from './benefit-value-prop-list/benefit-value-prop-list.component';

describe('BenefitsSectionComponent', () => {
  let component: BenefitsSectionComponent;
  let fixture: ComponentFixture<BenefitsSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BenefitsSectionComponent,
        BenefitsImageComponent,
        BenefitValuePropListComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
