import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerServiceService {
  constructor() {}

  /**
   * init
   */
  public init() {
    this.listenForSelection();
    this.listenForAnchorAction();
  }

  /**
   * listenForSelection
   */
  private listenForSelection() {
    document.addEventListener('selectionchange', function(x) {
      const selectionObj = window.getSelection(),
            selection = selectionObj.toString(),
            currentDate = new Date(),
            currentTimeStamp = currentDate.getTime();

      if (selection.length > 0) {
        console.log('text selected: ', {
          text: selection,
          timeSinceLoad: x.timeStamp,
          timestamp: currentTimeStamp
        });
      }
    });
  }

  /**
   * listenForHover
   */
  private listenForAnchorAction() {
    const anchors = document.querySelectorAll('a');

    for (let i = 0, n = anchors.length; i < n; i++) {
      this.listenForHover(anchors[i]);
      this.listenForClick(anchors[i]);
    }
  }

  private listenForHover(anchor) {
    anchor.addEventListener('mouseenter', this.logAnchorAction, false);
  }

  private listenForClick(anchor) {
    anchor.addEventListener('click', this.logAnchorAction, false);
  }

  private logAnchorAction(event) {
    let text = (<HTMLInputElement>event.currentTarget).outerText;
    const title = (<HTMLInputElement>event.currentTarget).getAttribute('data-title');

    if (text.length === 0 && title != null) {
      text = title;
    }

    console.log(`link ${event.type}: `, {
      text: text,
      link: (<HTMLAnchorElement>event.currentTarget).href
    });
  }
}
