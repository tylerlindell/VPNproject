import { SocialConnectModule } from './social-connect.module';

describe('SocialConnectModule', () => {
  let socialConnectModule: SocialConnectModule;

  beforeEach(() => {
    socialConnectModule = new SocialConnectModule();
  });

  it('should create an instance', () => {
    expect(socialConnectModule).toBeTruthy();
  });
});
