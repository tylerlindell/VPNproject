import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialConnectComponent } from './social-connect.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SocialConnectComponent]
})
export class SocialConnectModule { }
