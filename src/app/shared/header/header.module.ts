import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CallToActionModule } from '../../home/call-to-action/call-to-action.module';

@NgModule({
  imports: [
    CommonModule,
    CallToActionModule
  ],
  declarations: [HeaderComponent, NavbarComponent],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
