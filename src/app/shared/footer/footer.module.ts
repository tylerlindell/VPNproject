import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { FooterNavbarComponent } from './footer-navbar/footer-navbar.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FooterComponent, FooterNavbarComponent],
  exports: [
    FooterComponent
  ]
})
export class FooterModule { }
