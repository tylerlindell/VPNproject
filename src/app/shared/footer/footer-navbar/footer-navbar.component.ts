import { Component, OnInit } from '@angular/core';
import { SocialSites } from '../../../models/social-sites.model';

@Component({
  selector: 'app-footer-navbar',
  templateUrl: './footer-navbar.component.html',
  styleUrls: ['./footer-navbar.component.scss']
})
export class FooterNavbarComponent implements OnInit {
  public links = [
    { url: '#', text: 'PRODUCTS' },
    { url: '#', text: 'RESOURCES' },
    { url: '#', text: 'COMPANY' },
    { url: '#', text: 'SUPPORT' },
    { url: '#', text: 'SOCIAL:' }
  ];

  public socialSites: SocialSites[];
  public innerWidth = window.innerWidth;

  constructor() {}

  ngOnInit() {
    this.socialSites = [
      {
        src: '../../../assets/social_facebook.svg',
        url: 'https://www.facebook.com/goldenfrogsoftware',
        title: 'Facebook'
      },
      {
        src: '../../../assets/social_twitter.svg',
        url: 'https://twitter.com/golden_frog',
        title: 'Twitter'
      },
      {
        src: '../../../assets/social_linkedin.svg',
        url: 'https://www.linkedin.com/company/golden-frog',
        title: 'LinkedIn'
      }
    ];
  }
}
