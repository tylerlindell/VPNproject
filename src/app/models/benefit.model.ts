import { Url } from 'url';

export class Benefit {
  image: string;
  title: string;
  body: string;
  list: string[];
  listIcon?: string;
}
